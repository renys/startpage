# Startpage

I don't think I'm proud of this enough to show on r/startpages. It's mostly meant for my use, but if you want to use it, modify `scripts/config.js` as you need. Live preview [here.](https://aleph.coffee/startpage/)

## Preview
![What the site looks like](/repository/preview.png)
## Demo
![A demo of the site](/repository/demo.gif)