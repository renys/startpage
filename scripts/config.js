const links = {
	"school": [
				["https://ontariotechu.ca/mycampus/", "MyCampus"],
				["https://learn.ontariotechu.ca", "Canvas"],
				["https://mail.uoit.net", "School Mail"]
			  ],
	"lecture":[
				["https://meets.google.com", "sarny"],
				["https://meets.google.com", "warny"],
				["https://meets.google.com", "nyarny"],
				["https://meets.google.com", "parny"],
			  ],
	"sns":	  [
				["https://tweetdeck.twitter.com", "TweetDeck"],
				["https://tumblr.com", "Tumblr"],
				["https://reddit.com", "Reddit"],
				["https://twitch.tv", "Twitch"]
			  ],
	"work":	  [
				["https://github.com", "GitHub (don't use)"],
				["https://gitlab.com", "GitLab (better)"],
				["https://mail.protonmail.com/login", "ProtonMail"],
				["https://tomato-timer.com/", "Pomodoro Timer"]
			  ],
}

