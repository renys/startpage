document.getElementById("date").innerHTML = updateClock();

// please don't let me work with vanilla javascript ever again

function updateClock() {
  const current = new Date();
  const days = ['일', '월', '화', '수', '목', '금', '토'];

  const date = current.getFullYear()+'년'+(current.getMonth()+1)+'월'+current.getDate()+'일 ('+days[current.getDay()]+')';
  
  let minutes;
  if (current.getMinutes() < 10) {
    minutes = '0' + current.getMinutes().toString();
  } else {
    minutes =  current.getMinutes().toString();
  }

  let seconds = current.getSeconds();
  if (current.getSeconds() < 10) {
    seconds = '0' + current.getSeconds().toString();
  }

  // why does this work
  const time = current.getHours()+':'+minutes+':'+seconds;
  const oneul = date + ' ' + time;

  document.getElementById("date").innerHTML = oneul;
  setTimeout(updateClock, 1000);
}

updateClock();
