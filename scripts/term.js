helpMessage = "<p><b>Help</b></p>" + 
			  "<p>ls/cat [group]: Shows links available from that group. They're the same for this site.</p>" +
			  "<p>ddg/d [search term]: Searches DuckDuckGo.</p>" +
			  "<p>google/g [search term]: Searches Google.</p>" +
			  "<p>youtube/y [search term]: Searches YouTube.</p>" +
			  "<p>help: Shows this message.</p>";

function onEnter() {
	// check if enter's pressed
	// enter is keycode 13...
	if (event.keyCode === 13) {
		var command = document.getElementById("cli-input")
			.value
			.toLocaleLowerCase()
			.split(" "); 
		
		document.getElementById("cli-input").value = "";

		processCommand(command);
	}
}

function showGroups() {
	let counter = 0;
	let groupsList = '<table style="width:100%"> <tr>';
	for (const key in links) {
		groupsList = groupsList + '<td>' + key + '</td>';
		counter += 1;
		if (counter === 2) {
			groupsList += '</tr> <tr>';
			counter = 0;
		}
	}
	groupsList += '</tr> </table>';

	return groupsList;
}

function createLink(label, link) {
	if (label != null && link != null) {
		return "<a href=" + link + ">" + label + "</a>";
	} else {
		return "";
	}
}

function createLinkTable(group) {
	let counter = 0;
	let linksList = '<table style="width:100%"> <tr>';
	if (links.hasOwnProperty(group)) {
		console.log(links[group]);
		// TODO: refactor this when i'm not hungry
		for (let i = 0; i < links[group].length; i++) {
			linksList = linksList + '<td>' + createLink(links[group][i][1], links[group][i][0]) + '</td>';
			counter += 1;

			if (counter === 2) {
				linksList += '</tr> <tr>';
				counter = 0;
			}
		}	

		linksList += '</tr> </table>'
	} else {
		linksList = "";
	}

	return linksList
}

// why is javascript such a pain
// return void; let id links be modified
function processCommand(cmd) {
	let output = "";
	switch(cmd[0]) {
		case "ls":
		case "cat":
			output = createLinkTable(cmd[1]);
			break;
		case "ddg":
		case "d":
			cmd.shift();
			searchTerm = cmd.join(" ");
			window.open('https://duckduckgo.com/?q=' + searchTerm);
			break;
		// why
		case "google":
		case "g":
			cmd.shift();
			searchTerm = cmd.join(" ");
			window.open('https://google.com/search?q=' + searchTerm);
			break;
		case "youtube":
		case "y":
			cmd.shift();
			searchTerm = cmd.join(" ");
			window.open('https://www.youtube.com/results?search_query=' + searchTerm);
			break;
		case "help":
			output = helpMessage;
		default:
			break;
	}

	document.getElementById("links").innerHTML = output;
}

window.onload = () => {
	// Show the groups of links.
	// haha, innerHTML go brrrrrrrrrr
	document.getElementById("groups").innerHTML = showGroups();
	// document.getElementById("links").innerHTML = helpMessage;
}
